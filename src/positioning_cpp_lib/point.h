#ifndef POINT_H
#define POINT_H


class Point{

    
	public:
		Point(double x, double y);
		Point();
		void setX(double x);
        void setY(double y);
        double getX();
        double getY();

        bool operator == (Point &p);
        void operator = (Point &p);

	private:
        double x;
        double y;
		
};




#endif
