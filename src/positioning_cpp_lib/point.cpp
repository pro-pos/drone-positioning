#include "point.h"

Point::Point (double x, double y){
    this->x = x;
    this->y = y;
}



void Point::setX(double x){
    this->x = x;
}

void Point::setY(double y){
    this->y = y;
}

double Point::getX(){
    return this->x;
}

double Point::getY(){
    return this->y;
}

Point::Point() {
    this->x = 0;
    this->y = 0;
}

bool Point::operator==(Point &p) {
    return p.getX() == this->x && p.getY() == this->y;
}

void Point::operator=(Point &p) {
    this->x = p.getX();
    this->y = p.getY();
}
