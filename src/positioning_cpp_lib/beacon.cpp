#include "beacon.h"

Beacon::Beacon(int x, int y, unsigned int id){
	this->_x = x;
	this->_y = y;
	this->_id = id;
}

int Beacon::getPosX(){
	return this->_x;
}

int Beacon::getPosY(){
	return this->_y;
}

unsigned int Beacon::getId(){
	return this->_id;
}

void Beacon::setRadius(double radius){
	this->_radius = radius;
}

double Beacon::getRadius(){
	return this->_radius;
}

bool Beacon::operator==(Beacon &b){
	return this->getRadius() == b.getRadius();
}

bool Beacon::operator<(Beacon &b){
	return this->getRadius() < b.getRadius();
}
