#include "circle.h"

#define PI 3.14159265358979323846
#include <cstdio>

Circle::Circle(){

}

void Circle::intersect (Beacon beacon_a, Beacon beacon_b, Point &point_a, Point &point_b){
    double radius1 = beacon_a.getRadius();
    double radius2 = beacon_b.getRadius();

    double dx = beacon_b.getPosX() - beacon_a.getPosX();
    double dy = beacon_b.getPosY() - beacon_a.getPosY();
    double D = round(sqrt(pow(dx, 2) + pow(dy, 2)));

    if(D > radius1 + radius2){
        point_a.setX(-1);
        point_a.setY(-1);
        point_b.setX(-1);
        point_b.setY(-1);
    }
    else if(D < fabs(radius2 - radius1)){
        point_a.setX(-1);
        point_a.setY(-1);
        point_b.setX(-1);
        point_b.setY(-1);
    }
    else if(D == 0 && radius1 == radius2){
        point_a.setX(-1);
        point_a.setY(-1);
        point_b.setX(-1);
        point_b.setY(-1);
    }
    else{
        if ( D == radius1 + radius2 || D == radius1 - radius2){
            // the circles intersect at a single point
        }
        else{
            // the circles intersect a two points
        }

        double chorddistance = (pow(radius1, 2) - pow(radius2, 2) + pow(D, 2))/(2*D);

        // distance from 1st circle's centre to the chord between intersects
        double halfchordlength = sqrt(pow(radius1, 2) - pow(chorddistance, 2));
        double chordmidpointx = beacon_a.getPosX() + (chorddistance*dx)/D;
        double chordmidpointy = beacon_a.getPosY() + (chorddistance*dy)/D;

        double p1x = round(chordmidpointx + (halfchordlength*dy)/D);
        double p1y = round(chordmidpointy - (halfchordlength*dx)/D);

        double theta1 = round(degrees(atan2(p1y-beacon_a.getPosY(), p1x-beacon_a.getPosX())));

        double p2x = round(chordmidpointx - (halfchordlength*dy)/D);
        double p2y = round(chordmidpointy + (halfchordlength*dx)/D);

        double theta2 = round(degrees(atan2(p2y-beacon_a.getPosY(), p2x-beacon_a.getPosX())));

        if (theta2 > theta1){
            double temp1 = p1x;
            double temp2 = p1y;
            p1x = p2x;
            p1y = p2y;
            p2x = temp1;
            p2y = temp2;
        }
        

        point_a.setX(p1x);
        point_a.setY(p1y);
        point_b.setX(p2x);
        point_b.setY(p2y);
    }
}

double Circle::degrees(double radian){
    return(radian * (180/PI)); 
} 
