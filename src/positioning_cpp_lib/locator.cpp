#include "locator.h"

#include <bits/stdc++.h> 

Locator::Locator(){

}

double Locator::getPosX(){
	return _x;
}

double Locator::getPosY(){
	return _y;
}

void Locator::calculatePosition(){
	//Sort the beacons on radius
	this->sortBeacons();
	this->getIntersections();
	this->determineClosestCoords();
	
	for(int i = 0; i < AMOUNTOFFINALPOINTS; i++){
		printf("X: %f\n", arrayOfFinalPoints[i].getX());
		printf("Y: %f\n", arrayOfFinalPoints[i].getY());
	}
	
	//Calculate the position
	this->calculateCentroid();
}

void Locator::determineClosestCoords() {
    Point meanCoords[3];

    // set all points to zero
    for(auto & arrayOfFinalPoint : arrayOfFinalPoints){
        arrayOfFinalPoint.setX(0);
        arrayOfFinalPoint.setY(0);
    }

    // New point in the middle of coordinate pairs
    for(int i = 0; i < 3; i++){
        if(arrayOfPoints[i * 2].getX() != -1){
            meanCoords[i].setX((arrayOfPoints[i * 2].getX() + arrayOfPoints[i * 2 + 1].getX()) / 2);
            meanCoords[i].setY((arrayOfPoints[i * 2].getY() + arrayOfPoints[i * 2 + 1].getY()) / 2);
        }
    }

    // TODO: uitzoeken hoe je omgaat met minder dan 3 coordinaten

    // choose the right coordinate according to the distance to the meanpoints
    for(int i = 0; i < 3; i++){
        double pyth1 = 0;
        double pyth2 = 0;
        for(int x = 0; x < 3; x++){
            if(x != i){
                pyth1 += pythagoras(arrayOfPoints[i * 2], meanCoords[x]);
                pyth2 += pythagoras(arrayOfPoints[i * 2 + 1], meanCoords[x]);
            }
        }

        if(pyth1 < pyth2){
            arrayOfFinalPoints[i] = arrayOfPoints[i * 2];
        }
        else{
            arrayOfFinalPoints[i] = arrayOfPoints[i * 2 + 1];
        }
    }


}

void Locator::getIntersections(){
	// TODO: find intersection points between the circles, output is a bunch of coordinates of the intersections

    // set all points to zero
    for(auto & arrayofPoint : arrayOfPoints){
        arrayofPoint.setX(0);
        arrayofPoint.setY(0);
    }

    // find all intersection coordinates
	//printf("%i,%i\n", _beacons[0].getPosX(), _beacons[0].getPosY());
    getIntersection(_beacons[0], _beacons[1], arrayOfPoints[0], arrayOfPoints[1]);
    getIntersection(_beacons[1], _beacons[2], arrayOfPoints[2], arrayOfPoints[3]);
    getIntersection(_beacons[2], _beacons[0], arrayOfPoints[4], arrayOfPoints[5]);

}

void Locator::getIntersection(Beacon a, Beacon b, Point &point_a, Point &point_b){
	// TODO: finds the intersection between the radiusses of two beacons,
	// returns (0,1,2) to tell if there are intersections
    Circle calculator;
    calculator.intersect(a, b, point_a, point_b);

}

/*void Locator::findRadiuses(Beacon *beaconArray){
	int length = sizeof(beaconArray) / sizeof(*beaconArray);
	for (int i = 0; i < length; i++)
	{
		// TODO get sensor data
		double value = 0;
		Beacon *pointer = beaconArray + i;
		pointer->setRadius(value); 
	}
}*/

void Locator::addBeacon(int x, int y, unsigned int id){
	Beacon b = Beacon(x,y,id);
	this->_beacons.push_back(b);
}

bool Locator::setRadius(unsigned int beaconId, double radius){
	for (auto & _beacon : this->_beacons){
		if(_beacon.getId() == beaconId){
			_beacon.setRadius(radius);
			return true;
		}
	}
	return false;
}

void Locator::sortBeacons(){
	sort(this->_beacons.begin(), this->_beacons.end()); 
}

double Locator::pythagoras(Point a, Point b) {
    return sqrt(pow(abs(a.getX() - b.getX()), 2) + pow(abs(a.getY() - b.getY()), 2));
}

void Locator::calculateCentroid(){
	double sum_x = 0;
	double sum_y = 0;
	
	for(int i = 0; i < AMOUNTOFFINALPOINTS; i++){
		sum_x += arrayOfFinalPoints[i].getX();
		sum_y += arrayOfFinalPoints[i].getY();
	}

	this->_x = sum_x / AMOUNTOFFINALPOINTS;
	this->_y = sum_y / AMOUNTOFFINALPOINTS;
}
