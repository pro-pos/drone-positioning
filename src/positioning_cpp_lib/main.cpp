#include "locator.h"
#include <cstdio>
#include <chrono>
#include <thread>
#include <random>

//#include <windows.h>


/* Simulated drone position = (400,300)
 *
 * Beacon positions:
 * b1 = (200,200)
 * b2 = (200,500)
 * b3 = (1000,200)
 * 
 * Beacons:
 * (y)
 * |	b2
 * |
 * |			drone
 * |
 * |	b1					b3
 * O-------------------------- (x)
 *
 * Distances:
 * b1 -> drone = 223.61
 * b2 -> drone = 282.84
 * b3 -> drone = 608.28
 */

#define DISTANCE_ERROR_DEVIATION 5
const double distances[3] = {223.61, 282.84, 608.28};


double fRand(double fMin, double fMax){
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}


int main(){
	Locator l = Locator();

	l.addBeacon(200, 200, 0);
	l.addBeacon(200, 500, 1);
	l.addBeacon(1000, 200, 2);
	
	while(1){
		for(int i = 0; i < 3; i++){
			double radius = distances[i] + fRand(0, DISTANCE_ERROR_DEVIATION);
			l.setRadius(i, radius);
			printf("R = %f\n", radius);
		}
		l.calculatePosition();

		double x = l.getPosX();
		double y = l.getPosY();

		printf("Position: (%f,%f)\n", x, y);
	

		//Sleep
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	

		//Windows?
        //Sleep(1000);
        // random if statement to make while loop not endless but endless
        /*if(x == -2){
            break;
        }*/
	}


	return 1;
} 
