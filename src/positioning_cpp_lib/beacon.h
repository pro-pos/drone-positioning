#ifndef BEACON_H
#define BEACON_H

class Beacon{
	public:
		Beacon(int x, int y, unsigned int id);
		int getPosX();
		int getPosY();
		unsigned int getId();
		void setRadius(double radius);
		double getRadius();

		bool operator == (Beacon &b);
		bool operator < (Beacon &b);

	private:
		int _x;
		int _y;
		unsigned int _id;
		double _radius;
};


#endif
