import matplotlib
saveAsPGF = False
#set configuration to also save as pgf
if saveAsPGF:
    matplotlib.use("pgf")
    matplotlib.rcParams.update({
        "pgf.texsystem": "pdflatex",
        'font.family': 'serif',
        'text.usetex': True,
        'pgf.rcfonts': False,
    })
from matplotlib import pyplot as plt



filenames = list()
distances = [25, 50, 75, 100, 200, 300, 400, 500, 600, 700]

filenames.append("25cm.txt")
filenames.append("50cm.txt")
filenames.append("75cm.txt")
filenames.append("100cm.txt")
filenames.append("200cm.txt")
filenames.append("300cm.txt")
filenames.append("400cm.txt")
filenames.append("500cm.txt")
filenames.append("600cm.txt")
filenames.append("700cm.txt")


rssiValues = list()
rssiUpperError = list()
rssiLowerError = list()

for filename in filenames:
    #Open the file and create list
    log = open(filename, "r").read()
    log = log.split('\n')

    #Remove the last line
    log = log[0:-1]

    measurementNumber = list()
    milliseconds = list()
    rssi = list()
    distance = list()

    for i, line in enumerate(log):
        items = line.split(' ')

        measurementNumber.append(int(items[0]))
        milliseconds.append(int(items[1]))
        rssi.append(float(items[2]))
        distance.append(float(items[3]))



    #Calculate averages
    averageRSSI = sum(rssi) / len(rssi)
    rssiValues.append(averageRSSI)
    
    #Calculate error
    rssiLowerError.append(max(rssi) - averageRSSI)
    rssiUpperError.append(averageRSSI - min(rssi))

rssiError = [rssiUpperError, rssiLowerError]

#Plot the values
plt.title("RSSI measurements at incrementing distances")
plt.ylabel("RSSI (dBm)")
plt.xlabel("Distance in centimeters")
plt.errorbar(distances, rssiValues, yerr=rssiError, linestyle='None', marker='o')
plt.plot(distances, rssiValues)
if not saveAsPGF:
    plt.show()


#Save as pgf 
if saveAsPGF:
    plt.savefig('rssiplot.pgf')