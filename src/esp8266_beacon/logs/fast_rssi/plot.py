import matplotlib.pyplot as plt



filenames = list()
distances = list(range(10, 20))
distances += list(range(20, 205, 5))
distances += list(range(200, 510, 10))

for distance in distances:
    filenames.append(str(distance) + "cm.txt")






rssiValues = list()
distanceValues = list()
rssiUpperError = list()
rssiLowerError = list()

for filename in filenames:
    #Open the file and create list
    log = open(filename, "r").read()
    log = log.split('\n')

    #Remove the first few lines
    log = log[7:]

    #Remove the last line
    log = log[0:-1]

    measurementNumber = list()
    milliseconds = list()
    rssi = list()
    distance = list()

    for i, line in enumerate(log):
        items = line.split(' ')

        measurementNumber.append(int(items[0]))
        milliseconds.append(int(items[1]))
        distance.append(float(items[2]))
        rssi.append(float(items[3]))



    #Calculate averages
    averageRSSI = sum(rssi) / len(rssi)
    rssiValues.append(averageRSSI)

    averageDistance = sum(distance) / len(distance)
    distanceValues.append(averageDistance)
    
    #Calculate error
    rssiLowerError.append(max(rssi) - averageRSSI)
    rssiUpperError.append(averageRSSI - min(rssi))

rssiError = [rssiUpperError, rssiLowerError]




#Plot the values
plt.title("RSSI measurements at incrementing distances")
#plt.ylabel("RSSI (dBm)")
plt.xlabel("Distance in centimeters")

plt.errorbar(distances, rssiValues, yerr=rssiError, linestyle='None', marker='o', label="Min/max RSSI")
plt.plot(distances, rssiValues, label="Average RSSI (dBm)")

#plt.plot(distances, distanceValues, label="Average distance (cm)")


plt.legend()
plt.show()
