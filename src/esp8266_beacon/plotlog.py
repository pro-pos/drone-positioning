import matplotlib.pyplot as plt
import sys

args = sys.argv

if len(args) == 1:
    print("Usage: python plotlog.py filename")
    exit()

#Open the file and create list
filename = args[1]
log = open(filename, "r").read()
log = log.split('\n')

#Remove last line
log = log[:-1]


measurementNumber = list()
milliseconds = list()
rssi = list()
distance = list()

for i, line in enumerate(log):
    items = line.split(' ')

    measurementNumber.append(int(items[0]))
    milliseconds.append(int(items[1]))
    rssi.append(float(items[2]))
    distance.append(float(items[3]))



#Calculate averages
averageRSSI = [sum(rssi) / len(rssi)] * len(rssi)
averageDistance = [sum(distance) / len(distance)] * len(distance)



#Plot RSSI
plt.scatter(measurementNumber, rssi, label='RSSI')
plt.plot(measurementNumber, rssi)

#Plot distance
#plt.scatter(measurementNumber, distance)
#plt.plot(measurementNumber, distance)

#Plot averages
plt.plot(measurementNumber, averageRSSI, label='RSSI average')
#plt.plot(measurementNumber, averageDistance, label='Distance average')


plt.xlabel("Sample number")
plt.ylabel("RSSI (dBm)")
plt.legend()
plt.show()

#Print the averages
print("Average RSSI: " + str(averageRSSI[0]))
#print("Average Distance: " + str(averageDistance[0]))
