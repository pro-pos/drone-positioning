#!/bin/sh
git clone git@github.com:esp8266/Arduino.git
mv ./Arduino/libraries/ESP8266WiFi ./ESP8266WiFi
rm -rf ./Arduino
cd ./ESP8266WiFi
patch -p0 -i ../quickscan.patch
