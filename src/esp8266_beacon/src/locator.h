#ifndef LOCATOR_H
#define LOCATOR_H

#define AMOUNTOFPOINTS 6
#define AMOUNTOFFINALPOINTS 3

#include "beacon.h"
#include "circle.h"
#include <vector>

class Locator{
	public:
		Locator();
		double getPosX();
		double getPosY();
		void calculatePosition();
        void getIntersections();
		static void getIntersection(Beacon a, Beacon b, Point &point_a, Point &point_b);
		void addBeacon(int x, int y, unsigned int id);
		bool setRadius(unsigned int beaconId, float radius);
		float getRadius(unsigned int beaconId);
		void determineClosestCoords();
		static double pythagoras(Point a, Point b);
		void findRadiuses(Beacon *beaconArray);

	private:
		void sortBeacons();
		void calculateCentroid();
        Point arrayOfPoints[AMOUNTOFPOINTS];
        Point arrayOfFinalPoints[AMOUNTOFFINALPOINTS];
		
		double _x = 0;
		double _y = 0;


		std::vector<Beacon> _beacons;
};




#endif
