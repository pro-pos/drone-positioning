#ifdef ESP32S
//ESP32S uses another library for WIFI

#include "WiFi.h"

#else

//ESP8266-01
#include "ESP8266WiFi.h"
#include "locator.h"
#include "beaconscanner.h"

#endif

//Beacon variables
#define BEACON_SSID "Beacon"
#define BEACON_PASSWORD "PasswordBeacon"
#define OUTPUT_POWER_DBM 20.5

//Variables for distance calculations
#define ENV_FACTOR 2

//Pin defines
#define LED_PIN 1 //LED_BUILTIN

//Other defines
#define LED_BLINK_INTERVAL 500
#define MEASURE_START_DELAY 2000

//Task variables
bool ledState = true;
unsigned long ledTaskMillis = 0;


//Scanner for the locator ESP8266-01
#if !BEACON
BeaconScanner scanner = BeaconScanner();
Locator l = Locator();
#endif


//Calculate a distance in centimeters based on the RSSI, measured power in dBm and environment factor
float rssiToDistance(double rssi, int measuredPower, int environmentFactor){
	double meters = pow(10, (measuredPower - rssi) / (10 * environmentFactor));
	return meters * 100;
}

//Toggle the LED
void changeLedState(){
	ledState = !ledState;
	digitalWrite(LED_PIN, ledState);
}

//A task used on the beacons to toggle the LED
void ledTask(){
	if(millis() >= ledTaskMillis + LED_BLINK_INTERVAL){
		changeLedState();

		//Wait some time before changing the state again
		ledTaskMillis = millis();
	}
}

void setup() {
	//Setup serial for logging
	Serial.begin(115200);

//Compile for beacon objects
#if BEACON
	#ifndef ESP32S
	//ESP32S uses a fixed output power
	WiFi.setOutputPower(OUTPUT_POWER_DBM);
	#endif
	
	//Setup WiFi soft AP
	WiFi.softAP(BEACON_SSID, BEACON_PASSWORD, CHANNEL);
	
	//Setup LED
	pinMode(LED_PIN, OUTPUT);
	digitalWrite(LED_PIN, ledState);
	ledTaskMillis = millis();
#else
	BeaconStruct b;
	
	//Setup ESP8266-01
	b.id = 0;
	b.channel = 3;
	b.measuredPower = -42;
	b.bssid[0] = 0x62;
	b.bssid[1] = 0x01;
	b.bssid[2] = 0x94;
	b.bssid[3] = 0x1c;
	b.bssid[4] = 0xea;
	b.bssid[5] = 0xbc;

	l.addBeacon(0, 0, b.id);
	scanner.addBeacon(b);


	//Setup OP6T(Not an ESP)
	b.id = 1;
	b.channel = 6;
	b.measuredPower = -42;
	b.bssid[0] = 0xe6;
	b.bssid[1] = 0x8f;
	b.bssid[2] = 0xa2;
	b.bssid[3] = 0x76;
	b.bssid[4] = 0x31;
	b.bssid[5] = 0x28;

	l.addBeacon(845, 0, b.id);
	scanner.addBeacon(b);


	//Setup ESP32S
	b.id = 2;
	b.channel = 3;
	b.measuredPower = -42;
	b.bssid[0] = 0x24;
	b.bssid[1] = 0x6f;
	b.bssid[2] = 0x28;
	b.bssid[3] = 0x0b;
	b.bssid[4] = 0x08;
	b.bssid[5] = 0x45;

	l.addBeacon(0, 869, b.id);
	scanner.addBeacon(b);
	
	//Wait some time before logging to serial
	delay(MEASURE_START_DELAY);
#endif
}

void loop(){
#if BEACON
	//Blink the LED if compiled for a BEACON
	ledTask();
#else
	//Scan for beacons
	scanner.scan();
	
	//Loop through the three beacons
	for(int i = 0; i < 3; i++){
		//Get RSSI and calculate distance
		int rssi = scanner.getBeaconRSSI(i);
		int measuredPower = scanner.getBeaconMeasuredPower(i);
		float distance = rssiToDistance(rssi, measuredPower, ENV_FACTOR);
		
		//Print some info
		Serial.print("Beacon ID: ");
		Serial.print(i);
		Serial.print(" | RSSI: ");

		if(scanner.beaconIsInRange(i)){
			Serial.print(rssi);

			//Set the radius of the beacon
			l.setRadius(i, distance);
		}else{
			Serial.print("Not in range");
		}

		Serial.print(" | Distance: ");
		Serial.println(l.getRadius(i));
	}
	
	//Calculate the position
	l.calculatePosition();
	double x = l.getPosX();
	double y = l.getPosY();
	
	//Print the position
	Serial.print("Position: (");
	Serial.print(x);
	Serial.print(",");
	Serial.print(y);
	Serial.println(")");

#endif
}
