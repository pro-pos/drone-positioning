#ifndef BEACON_H
#define BEACON_H

#define HISTORY_SIZE 100

class Beacon{
	public:
		Beacon(int x, int y, unsigned int id);
		int getPosX();
		int getPosY();
		unsigned int getId();
		void setRadius(float radius);

		float getRadius();
		float _radius = 0;
	private:
		int _x;
		int _y;
		unsigned int _id;
		
		int _radiusIndex = 0;
		float _radiusses[HISTORY_SIZE] = {0};
};


#endif
