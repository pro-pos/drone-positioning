#ifndef ESP32S

#include "beaconscanner.h"
#include "ESP8266WiFi.h"

//Constructor for beacon scanner object
BeaconScanner::BeaconScanner(){
	WiFi.mode(WIFI_STA);
	WiFi.disconnect();
	
	//Set scanning the channel to false by default
	for(int i = 0; i < MAX_CHANNEL; i++){
		this->_channels[i] = false;
	}
}

//Add a beacon to the vector and enable scanning on the beacon's channel
void BeaconScanner::addBeacon(BeaconStruct beacon){
	this->_beacons.push_back(beacon);
	this->_channels[beacon.channel-1] = true;
}

//Scan for beacons and store the RSSI
void BeaconScanner::scan(){
	//Reset the values to indicate which beacons are found
	this->resetRSSIValues();

	//Only scan the channels on which the beacons should be visible
	for(int i = 0; i < MAX_CHANNEL; i++){
		if(this->_channels[i]){
			//Scan the channel
			this->scanChannel(i+1);
		}
	}
}

//Return the amount of beacons in the vector
uint8_t BeaconScanner::getBeaconCount(){
	return this->_beacons.size();
}

//Scan for beacons on a certain channel
void BeaconScanner::scanChannel(uint8_t channel){
	//Scan the networks
	int n = WiFi.scanNetworks(false, false, channel, NULL, false);

	//Compare every AP with the beacons
	for(int i = 0; i < n; ++i){
		
		//Check every added beacon
		for(int j = 0; j < this->_beacons.size(); j++){

			//Check channel of the beacon first
			if(this->_beacons.at(j).channel == channel){

				//Check if BSSID matches
				if(this->BSSIDEquals(this->_beacons.at(j).bssid, 
							WiFi.BSSID(i))){
					//Update the beacon; BSSID matches
					this->updateBeacon(j, i);
				}
			}
		}
	}
}



//Get the RSSI stored in a beacon in the beacons vector
int BeaconScanner::getBeaconRSSI(uint8_t beaconId){
	//Check every beacon
	for(int i = 0; i < this->_beacons.size(); i++){
		//If beacon exists, return the rssi value
		if(this->_beacons.at(i).id == beaconId){
			return this->_beacons.at(i).rssi;
		}
	}
	return RSSI_ERROR;
}

//Get the measured power in dBm for a beacon in the beacons vector
int BeaconScanner::getBeaconMeasuredPower(uint8_t beaconId){
	//Check every beacon
	for(int i = 0; i < this->_beacons.size(); i++){
		//If beacon exists, return the rssi value
		if(this->_beacons.at(i).id == beaconId){
			return this->_beacons.at(i).measuredPower;
		}
	}
	return 0;
}

//Check if the beacon is in range/is found
bool BeaconScanner::beaconIsInRange(uint8_t beaconId){
	int rssi = this->getBeaconRSSI(beaconId);
	
	//Check if the RSSI of the beacon has an error
	return rssi != RSSI_UNDEFINED
		&& rssi != RSSI_ERROR 
		&& rssi != RSSI_SCAN_ERROR;
}

//Reset the RSSI values of all beacons
void BeaconScanner::resetRSSIValues(){
	for(int i = 0; i < this->_beacons.size(); i++){
		this->_beacons.at(i).rssi = RSSI_UNDEFINED;
	}
}

//Update the RSSI for a beacon
void BeaconScanner::updateBeacon(uint8_t beaconIndex, uint8_t apIndex){
	this->_beacons.at(beaconIndex).rssi = WiFi.RSSI(apIndex);
}

//Check if an integer value is between a and b
bool BeaconScanner::isBetween(int value, int a, int b){
	return (value >= a && value <= b);
}

//Check if the BSSID/MAC equals another BSSID/MAC
bool BeaconScanner::BSSIDEquals(uint8_t a[6], uint8_t b[6]){
	for(int i = 0; i < 6; i++){
		if(a[i] != b[i]){
			return false;	
		}
	}
	return true;
}
#endif
