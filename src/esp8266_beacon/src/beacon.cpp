#include "beacon.h"

Beacon::Beacon(int x, int y, unsigned int id){
    // create new Beacon object with x, y and an ID
	this->_x = x;
	this->_y = y;
	this->_id = id;
}

int Beacon::getPosX(){
    // return x position
	return this->_x;
}

int Beacon::getPosY(){
    // return y position
	return this->_y;
}

unsigned int Beacon::getId(){
    // return beacon ID
	return this->_id;
}

void Beacon::setRadius(float radius){
    // set beacon radius
	this->_radiusses[this->_radiusIndex] = radius;

	// add to the current set-index
	this->_radiusIndex++;
	if(this->_radiusIndex >= HISTORY_SIZE){
		this->_radiusIndex = 0;
	}

	// calculating mean of radiusses for an exacter measurement
	float sum;
	for(int i = 0; i < HISTORY_SIZE; i++){
		sum += this->_radiusses[i];
	}

	this->_radius = sum / HISTORY_SIZE;
}

float Beacon::getRadius(){
    // return radius of beacon
	return this->_radius;
}
