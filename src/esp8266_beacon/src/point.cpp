#include "point.h"

Point::Point (double x, double y){
    // make an object with x and y
    this->x = x;
    this->y = y;
}

void Point::setX(double x){
    // set points x
    this->x = x;
}

void Point::setY(double y){
    // set points y
    this->y = y;
}

double Point::getX(){
    // get points x
    return this->x;
}

double Point::getY(){
    // get points y
    return this->y;
}

Point::Point() {
    // make an empty point
    this->x = 0;
    this->y = 0;
}

bool Point::operator==(Point &p) {
    // operator to compare points
    return p.getX() == this->x && p.getY() == this->y;
}

void Point::operator=(Point &p) {
    // operator to replace point
    this->x = p.getX();
    this->y = p.getY();
}
