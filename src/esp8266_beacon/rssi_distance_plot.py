import matplotlib.pyplot as plt



filenames = list()
distances = [100, 150, 200, 250, 300, 400]

filenames.append("logs/distance_100cm_1.txt")
filenames.append("logs/distance_150cm_1.txt")
filenames.append("logs/distance_200cm_1.txt")
filenames.append("logs/distance_250cm_1.txt")
filenames.append("logs/distance_300cm_1.txt")
filenames.append("logs/distance_400cm_1.txt")


rssiValues = list()
rssiUpperError = list()
rssiLowerError = list()

for filename in filenames:
    #Open the file and create list
    log = open(filename, "r").read()
    log = log.split('\n')

    #Remove the first and last line
    log = log[1:-1]

    measurementNumber = list()
    milliseconds = list()
    rssi = list()
    distance = list()

    for i, line in enumerate(log):
        items = line.split(' ')

        measurementNumber.append(int(items[0]))
        milliseconds.append(int(items[1]))
        rssi.append(float(items[2]))
        distance.append(float(items[3]))



    #Calculate averages
    averageRSSI = sum(rssi) / len(rssi)
    rssiValues.append(averageRSSI)
    
    #Calculate error
    rssiUpperError.append(max(rssi) - averageRSSI)
    rssiLowerError.append(averageRSSI - min(rssi))

rssiError = [rssiUpperError, rssiLowerError]

#Plot the values
plt.title("RSSI measurements at incrementing distances")
plt.ylabel("RSSI (dBm)")
plt.xlabel("Distance in centimeters")
plt.errorbar(distances, rssiValues, yerr=rssiError, linestyle='None', marker='o')
plt.plot(distances, rssiValues)
plt.show()
