#ifndef ESP32S

#ifndef BEACON_SCANNER_H 
#define BEACON_SCANNER_H

#include <Arduino.h>
#include <stdint.h>
#include <vector>

//Static error values
#define RSSI_UNDEFINED 10
#define RSSI_ERROR 20
#define RSSI_SCAN_ERROR 30

//Max channel for the EU
#define MAX_CHANNEL 13 //EU etc.

//Define a beacon as a struct
struct BeaconStruct{
	uint8_t id;
	uint8_t channel;
	uint8_t bssid[6] = {0};
	int measuredPower;
	int rssi;
};

//The beacon class
class BeaconScanner{
	public:
		BeaconScanner();
		void addBeacon(BeaconStruct beacon);

		void scan();
		int getBeaconRSSI(uint8_t beaconId);
		int getBeaconMeasuredPower(uint8_t beaconId);
		bool beaconIsInRange(uint8_t beaconId);
		uint8_t getBeaconCount();

	private:
		void scanChannel(uint8_t channel);
		void resetRSSIValues();
		void updateBeacon(uint8_t beaconIndex, uint8_t apIndex);

		bool isBetween(int value, int a, int b);
		bool BSSIDEquals(uint8_t a[6], uint8_t b[6]);

		bool _channels[MAX_CHANNEL];
		String _ssid;
		std::vector<BeaconStruct> _beacons;
};

#endif
#endif
