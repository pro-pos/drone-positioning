#include <Arduino.h>
#include <printf.h>
#include <SPI.h>
#include "RF24.h"

//#define IS_BEACON
#define PLOT_TIME

#define TIMEOUT_US 5000
#define PROCESSING_TIME_US 640


RF24 radio(7,8);


uint8_t addresses[][6] = {"1Node","2Node"};

unsigned long sendTime = 0;
unsigned long receiveTime = 0;
unsigned long smallestDuration = TIMEOUT_US;

void master_sendPacket(){
	radio.stopListening();
	uint8_t data = 0;

	sendTime = micros();
	if(!radio.write(&data, sizeof(uint8_t))){
#ifndef PLOT_TIME
		Serial.println(F("TRANSMIT: failed"));
#endif
	}

	radio.startListening();
}

bool master_dataAvailable(){
	while(!radio.available()){
		if (micros() - sendTime > TIMEOUT_US){
			return false;
		}
	}
	return true;
}

void master_handleReceivedData(){
	receiveTime = micros();

	radio.flush_rx();

	//unsigned long receivedData = 0;
	//radio.read(&receivedData, sizeof(unsigned long));

	unsigned long rtt = receiveTime - sendTime;
	if(rtt < smallestDuration){
		smallestDuration = rtt;
	}

	unsigned long correctedRtt = (rtt - PROCESSING_TIME_US) / 2;
	unsigned long correctedRtt_record = (rtt - smallestDuration) / 2;
	
#ifdef PLOT_TIME
	Serial.println(rtt);
#else
	Serial.println("==========");
	Serial.print("Time: ");
	Serial.println(rtt);
	Serial.print("Corrected time: ");
	Serial.println(correctedRtt);
	Serial.print("Corrected time with record: ");
	Serial.println(correctedRtt_record);
#endif
}

void beacon_sendACK(){
	uint8_t data = 0;

	/*while(radio.available()){
		radio.read(&data, sizeof(unsigned long));
	}*/

	radio.stopListening();
	
	if(!radio.write(&data, sizeof(uint8_t))){
		Serial.println(F("TRANSMIT: failed"));
	}

	radio.flush_rx();

	radio.startListening();
}

void loopMaster(){
	master_sendPacket();

	if(master_dataAvailable()){
		master_handleReceivedData();
	}else{
		Serial.println(F("TRANSMIT: Failed, response timed out."));
	}
	
	delay(1000);
}

void loopBeacon(){
	if(radio.available()){
		beacon_sendACK();
		Serial.println(F("Sent response"));
	}
}

void setupMaster(){
	radio.openWritingPipe(addresses[1]);
	radio.openReadingPipe(1,addresses[0]);
}

void setupBeacon(){
	radio.openWritingPipe(addresses[0]);
	radio.openReadingPipe(1,addresses[1]);
}

void setup() {
	Serial.begin(115200);
	printf_begin();

	radio.begin();

#ifndef PLOT_TIME
	radio.printDetails();

	if(radio.failureDetected){
		Serial.println("RF hardware defective");
		return;
	}
#endif

	//Configure radio
	radio.setDataRate(RF24_2MBPS);
	radio.setPALevel(RF24_PA_MAX);
	radio.setAutoAck(false);
	radio.disableDynamicPayloads();
	radio.disableCRC();
	radio.setPayloadSize(1);
	//radio.setChannel(125);






	//Setup the correct device
#ifdef IS_BEACON
	setupBeacon();
#else
	setupMaster();
#endif
	

	//Start listening
	radio.startListening();
}

void loop() {
#ifdef IS_BEACON
	loopBeacon();
#else
	loopMaster();
#endif
}
