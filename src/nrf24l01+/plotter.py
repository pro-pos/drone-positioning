#!/usr/bin/env python3

import matplotlib.pyplot as plt

mean = lambda l : sum(l) / len(l)
std = lambda l : mean(l) /2
z_score = lambda x, mean, std : (x - mean_val)/std_val
z_threshold = 2

log = open("log.txt", "r").read()

log = log.split('\n')

for i, item in enumerate(log):
    if "TRANSMIT" in item or item == "":
        log[i] = 0
    else:
        log[i] = int(item)

mean_val = mean(log); print(mean_val)
std_val = std(log); print(std_val)

new_log = []

for index, val in enumerate(log):
    z_score_val = abs(z_score(val, mean, std))
    if z_score_val >= z_threshold:
        print("Outlier", val)
        log.pop(4)
    else:
        new_log += [val]

plt.plot(new_log)
plt.show()
