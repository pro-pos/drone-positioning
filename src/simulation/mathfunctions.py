"""Module to hold all maths functions related to determining the position"""

from math import exp

def weighted_average(weights, numbers):
    """Return avarage according weight given to every entry in an array"""
    number = 0

    for i in range(0, len(numbers)):
        number += weights[i] * numbers[i]

    return number / sum(weights)

def sigmoid(x):
    """A sigmoid function is used to give weight to location history entries of the drone
    Newer entries are more relevent and have therefore a higher value in the sigmoid function"""

    return 1 / (1 + exp(-x))

def rollList(a, n):
    """Roll list a, n amount of times a in circular buffer"""
    size = len(a)

    b = [0] * size

    for i in range(0, size):
        offset = (i + n) % size
        b[offset] = a[i]

    return b

def discrete_derivative(data, step_size=1):
    """Return a list of descrite dirivatives from a data list"""
    size = len(data)

    y_axis = [0] * size

    for i in range(1, size -1):
        index = (i + step_size) % size

        next_value = data[round(i + step_size) % size]
        previous_value = data[round(i - step_size) % size]

        result = (next_value - previous_value) / (2 * step_size)
        y_axis[i] = result

    return list(range(0, size)), y_axis
