"""Module contains locator class"""
import time
from random import randint

from point import *
from polygon import *
from circle import *
from mathfunctions import *

class Locator:
    """Class contains functions to determine locations of objects"""

    def __init__(self, point, beacons, simulated_error=0, fixed_simulated_error=False):
        self._beacons = beacons
        self._point = point

        self._simulated_error = simulated_error
        self._fixed_simulated_error = fixed_simulated_error

        #Location history
        self._history_size = 50
        self._last_history_index = 0

        self._location_history = [None] * self._history_size
        self._accuracy_history = [0] * self._history_size


    def calculate_radiuses(self):
        """Calculate radiuses of the beacons"""

        #Set the radiuses
        for b in self._beacons:
            if self._fixed_simulated_error:
                error = self._simulated_error
            else:
                error = randint(- self._simulated_error, self._simulated_error)

            b.set_radius(b.get_distance_to(self._point) + error)

        self._beacons = self._sort_beacons_on_radius(self._beacons)

    def _get_intersections(self):
        intersections = list()

        #Get all beacons and list the beacons in the following format: (beacon: (intersections...))
        for beacon in self._beacons:
            beacon.reset_intersections()
            for other_beacon in self._beacons:

                #Dont compare the beacon with the same beacon
                if beacon != other_beacon:

                    #Get intersections
                    result = beacon.intersect(other_beacon)

                    #No result: Intersect with a line between the two beacons
                    if result == None:
                        result = list()
                        result.append(beacon.intersect_line(other_beacon))
                        result.append(other_beacon.intersect_line(beacon))

                    #Append the intersections if they do not exist yet
                    for i in result:
                        beacon.add_intersection(i)

                        if i not in intersections:
                            intersections.append(i)

        return intersections

    def _sort_beacons_on_most_intersections(self, beacons):
        """Return sorted lost of beacons according most amount of intersections on that beacon"""
        return sorted(beacons, key=lambda x: len(x.get_intersections()), reverse=True)

    def _sort_beacons_on_radius(self, beacons):
        """Return sorted lost of beacons according to the radius"""
        return sorted(beacons, key=lambda x: x.get_radius())

    def draw_all_intersections(self, screen, pygame):
        """Draw all intersections from self to the pygame screen"""
        intersections = self._get_intersections()
        for i in intersections:
            i.draw(screen, pygame, "I")

    def _get_polygon_points(self):
        #All intersections
        intersections = self._get_intersections()

        #Sort list on most intersected
        beacons = self._sort_beacons_on_most_intersections(self._beacons)
        most_intersected_beacon = beacons[0]

        #Closest two intersections on the most intersected circle
        closest_intersections = most_intersected_beacon.get_closest_intersections()

        #Remove the closest intersections from the list
        for i in closest_intersections:
            intersections.remove(i)

        #Get the next closest intersections
        points = list()
        for i in closest_intersections:
            #Get closest point
            p = i.get_closest_point(intersections)

            #Append the points
            points.append(i)
            if p != None:
                points.append(p)

        return points

    def _get_min_error(self):
        error_values = list()
        for b in self._beacons:
            error_values.append(b.get_error())

        return min(error_values)

    def _get_max_error(self):
        error_values = list()
        for b in self._beacons:
            error_values.append(b.get_error())

        return max(error_values)

    def _get_average_error(self):
        """Return the average error value of all the beacons"""

        error_value = 0
        for b in self._beacons:
            error_value += b.get_error()

        return error_value / len(self._beacons)

    def calculate_location(self):
        """Set location of self according the to accuracy and exact* location"""
        points = self._get_polygon_points()
        if len(points) == 0:
            return

        poly = Polygon(points)
        location = poly.get_centroid()
        accuracy = round(self._get_average_error())

        self._set_location(location, accuracy)

        #TODO: remove this
        return points, poly

    def draw_location(self, screen, pygame):
        """Draw location on pygame screen"""

        points, poly = self.calculate_location()

        location = self.get_location()
        accuracy = self.get_accuracy()

        range_circle = Circle(location.get_pos(), accuracy)

        #for p in points:
        #    p.draw(screen, pygame, "P")

        #poly.draw(screen, pygame)
        range_circle.draw(screen, pygame, (255, 255, 0))
        location.draw(screen, pygame, 'C')

        for l in self._location_history:
            if l != None:
                l.draw(screen, pygame, color=(200, 200, 0))

    def get_location(self, index=0):
        """Return most recent location from the location history list"""
        index += self._last_history_index - 1
        return self._location_history[index]

    def get_accuracy(self, index=0):
        """Return most recent accuracy from the accuracy history list"""
        index += self._last_history_index - 1
        return self._accuracy_history[index]

    def _set_location(self, location, accuracy):
        if self._last_history_index == self._history_size:
            self._last_history_index = 0

        self._location_history[self._last_history_index] = location
        self._accuracy_history[self._last_history_index] = accuracy

        self._last_history_index += 1
