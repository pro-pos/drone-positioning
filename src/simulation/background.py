"""Module contains background class"""

import pygame

class Background(pygame.sprite.Sprite):
    """Class models background for pygame"""


    def __init__(self, image_file, location, size_x, size_y):
        #Create sprite
        pygame.sprite.Sprite.__init__(self)

        #Load image
        self.image = pygame.image.load(image_file)
        self.image = pygame.transform.scale(self.image, (size_x, size_y))

        self.rect = self.image.get_rect()
        self.rect.left, self.rect.top = location

    def draw(self, screen):
        """Draw self on screen"""
        screen.blit(self.image, self.rect)

        self.make_legenda(screen)

    def make_legenda(self, screen):
        """Draw legenda on pygame background"""
        pygame.draw.rect(screen, (0, 0, 0), (0, 0, 400, 140))

        font = pygame.font.SysFont('DejaVu Sans Mono', 15)
        text = font.render("BLUE CIRCLE:  Raw radius with error", True, (100,100,225))
        screen.blit(text, (0,0))
        text = font.render("RED CIRCLE:   Fast but inacurate radius", True, (255, 0, 0))
        screen.blit(text, (0,20))
        text = font.render("GREEN CIRCLE: Slow but accurate radius", True, (0, 255, 0))
        screen.blit(text, (0,40))
        text = font.render("B0, B1, B2: Beacons", True, (255, 255, 255))
        screen.blit(text, (0,60))
        text = font.render("B0, B1, B2: Beacons", True, (255, 255, 255))
        screen.blit(text, (0,60))
        text = font.render("C: Location history", True, (255, 255, 0))
        screen.blit(text, (0,80))
        text = font.render("YELLOW CIRCLE: Location error probablability", True, (255, 255, 0))
        screen.blit(text, (0,100))
        text = font.render("DRONE: Actuall drone position", True, (255, 255, 255))
        screen.blit(text, (0,120))
