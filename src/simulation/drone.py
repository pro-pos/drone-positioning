"""Module contains drone class"""

from locator import *

class Drone(Point):
    """Class models a virtual drone"""
    def __init__(self, movement_speed):
        super().__init__()
        #self._beacons = beacons
        self._locator = None
        self._movement_speed = movement_speed

    def move(self, x, y):
        """Move the drone object to a new position"""
        self._x += self._movement_speed * x
        self._y += self._movement_speed * y

    def get_movement_speed(self):
        """Return the speed of the drone,
        NOTE: conflicting with acceleration functions"""
        return self._movement_speed

    def draw(self, screen, pygame, color=(255, 255, 255)):
        """Draw self of pygame screen"""
        super().draw(screen, pygame, "Drone", color)

    #def locate(self):
    #    #beacons = copy.deepcopy(self._beacons)
    #    beacons = self._beacons

    #    self._locator = Locator(beacons, self)
    #    self._locator.calculateBeaconRadiuses()   #Get all beacons
    #    self._locator.drawBeaconRadiuses()  #Optional

    #    self._locator.locate(self._screen, self._pygame)      #Does not return anything
    #    location = self._locator.getLocation()
    #    accuracy = self._locator.getAccuracy()
    #    print("Location = " + str(location))
    #    print("Accuracy = " + str(accuracy))


    #def drawExpectedPosition(self):
    #    location = self._locator.getLocation()
    #    accuracy = round(self._locator.getAccuracy())

    #    #Draw center
    #    Drawable(self._screen, self._pygame, "C", location.get_pos()).draw()

    #    #Draw circle around center
    #    self._pygame.draw.circle(self._screen, (255,228,0), location.get_pos(), accuracy, 1)
