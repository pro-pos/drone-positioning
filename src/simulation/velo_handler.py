#!/usr/bin/env python

"""Module contains velocity handler class"""

import logging

logging.basicConfig(level=logging.INFO)

class VeloHandler:
    """Class to handle the velocity of an object"""

    def __log_velo(self):
        "Log velocity values"""
        msg = f'Velocities: N {self.velocity_north} '
        msg += f'E {self.velocity_east} '
        msg += f'S {self.velocity_south} '
        msg += f'W {self.velocity_west} '
        logging.info(msg)

    def dec_velo(self):
        """Decrement velocity"""
        if self.velocity_north - self.dec_amount >= 0:
            self.velocity_north = round(self.velocity_north - self.dec_amount, 3)
        if self.velocity_south - self.dec_amount >= 0:
            self.velocity_south = round(self.velocity_south - self.dec_amount, 3)
        if self.velocity_east - self.dec_amount >= 0:
            self.velocity_east = round(self.velocity_east - self.dec_amount, 3)
        if self.velocity_west - self.dec_amount >= 0:
            self.velocity_west = round(self.velocity_west - self.dec_amount, 3)

        self.__log_velo()

    def inc_velo(self, direction):
        """Increment velocity"""
        if direction == 'N': self.velocity_north += self.inc_amount
        elif direction == 'S': self.velocity_south += self.inc_amount
        elif direction == 'W': self.velocity_west += self.inc_amount
        elif direction == 'E': self.velocity_east += self.inc_amount
        self.__log_velo()

    def __init__(self):
        self.inc_amount = 0.1
        self.dec_amount = 0.02
        self.velocity_north = 0.0
        self.velocity_south = 0.0
        self.velocity_west = 0.0
        self.velocity_east = 0.0
