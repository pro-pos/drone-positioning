#!/usr/bin/env python

import sys
sys.path.append('../')

from mathfunctions import *

def test_weighted_average():
    assert 2.5 == weighted_average([1,1,1,1], [1,2,3,4])
    assert 2.0 == weighted_average([1,2,3,4], [4,3,2,1])

def test_sigmoid_function():
    assert 0.5 == sigmoid(0)
    assert 0.1 > sigmoid(-5)
    assert 0.9 < sigmoid(5)

# TODO not done
