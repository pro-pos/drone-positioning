#!/usr/bin/env python3

"""Module to run simulation code"""

import pygame
import time
from time import sleep

from drone import Drone
from beacon import Beacon
from velo_handler import VeloHandler
from locator import Locator
from background import Background

#Simulation Updates Per Second
UPS = 120

#Frames Per Second
FPS = 60

#Beacon Updates Per Second
BUPS = 10

#Pygame size
SIZE_X = 1280
SIZE_Y = 720

KEYSTROKE_TIMEOUT = 0.1

#Distance error for simulation
DISTANCE_ERROR_DEVIATION = 25
FIXED_ERROR = False

#Drone settings
MOVEMENT_SPEED = 5

def update_velocity(velo_handler):
    """update velocity according to keyboard input"""
    pressed = pygame.key.get_pressed()
    if pressed[pygame.K_w]: velo_handler.inc_velo('N')
    if pressed[pygame.K_a]: velo_handler.inc_velo('W')
    if pressed[pygame.K_s]: velo_handler.inc_velo('S')
    if pressed[pygame.K_d]: velo_handler.inc_velo('E')

def update_drone_movement(velo_handler, drone):
    """Decrease velocity in all directions and move drone according to velocity"""
    velo_handler.dec_velo()
    drone.move(velo_handler.velocity_east-velo_handler.velocity_west,
               velo_handler.velocity_south-velo_handler.velocity_north)

def main():
    """Main"""

    #Setup Beacons
    beacons = list()
    for i in range(0, 3):
        b = Beacon("B" + str(i), UPS, BUPS)
        beacons.append(b)

    beacons[0].set_pos((200, 200))
    beacons[1].set_pos((200, SIZE_Y - 200))
    beacons[2].set_pos((SIZE_X - 200, 200))

    #Create drone
    drone = Drone(MOVEMENT_SPEED)
    drone.set_pos((SIZE_X - 200, SIZE_Y - 200))

    #Create locator
    loc = Locator(drone,
                  beacons,
                  simulated_error=DISTANCE_ERROR_DEVIATION,
                  fixed_simulated_error=FIXED_ERROR)

    #Init pygame
    pygame.init()
    screen = pygame.display.set_mode((SIZE_X, SIZE_Y))
    floorplan = Background("floorplans/test2.png", [0, 0], SIZE_X, SIZE_Y)

    #Tickers
    draw_tick = 0
    locate_tick = 0

    #Loop the simulation
    paused = False
    draw_drone = True

    # create velocity handler
    velo_handler = VeloHandler()

    #Tickers
    draw_tick = 0
    locate_tick = 0

    while True:
        draw_tick += FPS/UPS
        locate_tick += BUPS/UPS

        #Check for events
        ev = pygame.event.poll()

        #Check for keyboard input
        pressed = pygame.key.get_pressed()

        #Exit the simulation
        if ev.type == pygame.QUIT: break
        if pressed[pygame.K_ESCAPE]: break

        #Handle movement keys
        #if pressed[pygame.K_w]:
        #    drone.move(0,-1)
        #if pressed[pygame.K_a]:
        #    drone.move(-1,0)
        #if pressed[pygame.K_s]:
        #    drone.move(0,1)
        #if pressed[pygame.K_d]:
        #   drone.move(1,0)

        #Pause the simulation
        if pressed[pygame.K_SPACE]:
            paused = not paused
            time.sleep(KEYSTROKE_TIMEOUT)

        #Toggle draw drone
        if pressed[pygame.K_RETURN]:
            draw_drone = not draw_drone
            time.sleep(KEYSTROKE_TIMEOUT)

        #Plot the radius history
        if pressed[pygame.K_p]:
            for b in beacons:
                b.plot_history()
            time.sleep(KEYSTROKE_TIMEOUT)

        update_velocity(velo_handler)
        update_drone_movement(velo_handler, drone)

        #Execute beacon update functions
        if not paused and locate_tick >= 1:
            locate_tick = 0

            #Locate the drone
            loc.calculate_radiuses()

        #Execute the draw functions
        if draw_tick >= 1:
            draw_tick = 0

            #Draw a black screen
            screen.fill((0, 0, 0))
            floorplan.draw(screen)

            #Draw intersections
            #loc.drawAllIntersections(screen, pygame)
            loc.draw_location(screen,pygame)

            #Draw the beacons
            for b in beacons:
                b.draw(screen, pygame, drawRadius=True)

            #Draw the drone if possible
            if draw_drone:
                drone.draw(screen, pygame)

            #Update the screen
            pygame.display.update()

        #Wait some time before looping again
        time.sleep(1/UPS)

if __name__ == '__main__':
    main()
