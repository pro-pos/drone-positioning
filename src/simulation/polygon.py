"""Module contains polygon class"""

from point import *

class Polygon:
    """Class models drawn polygon shapes that can be drawn on the pygame background"""

    def __init__(self, points):
        self._points = points

    def get_points(self):
        return self._points

    def get_centroid(self):
        N = len(self._points)
        if N == 0:
            return None
        elif N == 1:
            return self._points[0]

        sum_x = 0
        sum_y = 0
        for p in self._points:
            pos = p.get_pos()
            sum_x += pos[0]
            sum_y += pos[1]

        x = sum_x / N
        y = sum_y / N

        return Point((x, y))

    def draw(self, screen, pygame):
        points = list()
        for p in self._points:
            points.append(p.get_pos())

        color = (119, 90, 172)

        N = len(points)
        if N < 2:
            return
        elif N < 3:
            pygame.draw.line(screen, color, points[0], points[1])
        else:
            pygame.draw.polygon(screen, color, points, 2)
