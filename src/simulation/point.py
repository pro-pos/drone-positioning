"""Module to contain point class"""

from math import sqrt

class Point:
    """class models drawn point in pygame playfield"""

    def __init__(self, position=(0, 0)):
        self.set_pos(position)

    def set_pos(self, pos):
        """Set position of of the the point object"""
        self._x = round(pos[0])
        self._y = round(pos[1])

    def get_pos(self):
        """Return position of the point object"""
        return (round(self._x), round(self._y))

    def get_x(self):
        """Return x coordinate of the point object"""
        return self._x

    def get_y(self):
        """Return y coordinate of the point object"""
        return self._y

    def set_x(self, x):
        """Set x coordinate of the point object"""
        self._x = x

    def set_y(self, y):
        """Set y coordinate of the point object"""
        self._y = y

    def get_distance_to(self, other):
        """Get distance to other point object"""
        self_x, self_y = self.get_pos()
        other_x, other_y = other.get_pos()

        delta_x = abs(other_x - self_x)
        delta_y = abs(other_y - self_y)

        return sqrt((delta_x * delta_x) + (delta_y * delta_y))

    def get_closest_points(self, points):
        comparissons = list()
        for p in points:
            if self != p:
                distance = self.get_distance_to(p)
                comparissons.append((p, distance))

        #Sort and retrieve the lowest distance
        comparissons.sort(key=lambda x: x[1])

        #Remove the distances
        points = list()
        for c in comparissons:
            points.append(c[0])

        #Return the points in order of distance (low to high)
        return points

    def get_closest_point(self, points):
        points = self.get_closest_points(points)

        if len(points) == 0:
            return None

        return points[0]

    def get_furthest_point(self, points):
        points = self.get_closest_points(points)

        if len(points) == 0:
            return None

        return points[len(points) -1]


    def __eq__(self, other):
        if other == None:
            return False

        if self.get_pos() != other.get_pos():
            return False

        return True

    def __str__(self):
        return str(type(self)) + ": (" + str(self._x) + ", " + str(self._y) + ")"

    def draw(self, screen, pygame, name=None, color=(255, 255, 255)):
        #Draw name if possible
        if name != None:
            string = str(name) + " (" + str(self._x) + "," + str(self._y) + ")"
            textpos = (self._x + 5, self._y + 5)
            font = pygame.font.SysFont('DejaVu Sans Mono', 20)

            text = font.render(string, True, color)
            screen.blit(text, textpos)

        #Draw the point
        pygame.draw.circle(screen, color, self.get_pos(), 5)
