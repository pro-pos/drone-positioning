"""Module to contain beacon class"""

import matplotlib.pyplot as plt

from circle import *
from mathfunctions import *

class Beacon(Circle):
    """class models drawn circle beacon in pygame playfield"""

    def __init__(self, name, ups, bups, position=(0, 0)):
        super().__init__(position)
        self._name = name
        self._radius_history_size = 50
        self._min_max_history_size = 10

        self._ups = ups
        self._bups = bups

        #Create ring buffers
        self._radius_history = [1] * self._radius_history_size
        self._last_radius_index = 0

        self._min_radius_history = [0] * self._min_max_history_size
        self._max_radius_history = [0] * self._min_max_history_size

        self._last_min_max_index = 0

    def __str__(self):
        return f'Beacon {Circle.__str__(self)}'

    def draw(self, screen, pygame, color=(255, 255, 255), drawRadius=False):
        if self.get_radius() < 1:
            return

        #Draw center point
        center = Point(self.get_pos())
        center.draw(screen, pygame, self._name + " Err: " + str(self.get_error()), color)

        #Draw circle
        if drawRadius:
            #Almost realtime measurements
            last_radius = self._radius_history[self._last_radius_index - 1]
            #Average from the complete buffer
            average_radius = self.get_radius(False)
            #Average from the complete buffer weighted with a sigmoid function
            sigmoid_radius = self.get_radius(True)

            pygame.draw.circle(screen, (0, 0, 255), self.get_pos(), last_radius, 1)
            pygame.draw.circle(screen, (0, 255, 0), self.get_pos(), average_radius, 1)
            pygame.draw.circle(screen, (255, 0, 0), self.get_pos(), sigmoid_radius, 1)

    def set_radius(self, radius):
        """Overide circle 'set_radius' function"""
        if radius < 0:
            return

        radius = round(radius)
        super().set_radius(radius)

        if self._last_radius_index == self._radius_history_size:
            self._last_radius_index = 0
            self._update_error_history()

        self._radius_history[self._last_radius_index] = radius
        self._last_radius_index += 1

    def get_radius(self, apply_sigmoid=True):
        """Overide circle 'get_radius' function
        Get the average of the buffer"""

        if apply_sigmoid:
            #Determine center of the sigmoid (75% of the total buffer)
            sigmoid_center = self._radius_history_size - (self._radius_history_size / 4)

            #Create weights list and execute the sigmoid function on the list
            weights = [sigmoid(x - sigmoid_center) for x in range(0, self._radius_history_size)]

            #Roll the circular buffer
            weights = rollList(weights, self._last_radius_index - 1)

            #Return the weighted average
            return round(weighted_average(weights, self._radius_history))

        return round(sum(self._radius_history) / self._radius_history_size)

    def _update_error_history(self):
        """Update the error history for calculating the average later"""
        if self._last_min_max_index == self._min_max_history_size:
            self._last_min_max_index = 0

        self._min_radius_history[self._last_min_max_index] = min(self._radius_history)
        self._max_radius_history[self._last_min_max_index] = max(self._radius_history)

        self._last_min_max_index += 1

    def get_error(self, index=None):
        """Get the average error"""
        if not index:
            #Return the realtime error
            return (max(self._radius_history) - min(self._radius_history)) / 2

        #Return error from history
        index += self._last_min_max_index-1
        return (self._max_radius_history[index] - self._min_radius_history[index]) / 2

    def get_relative_velocity(self):
        """Get the relative velocity"""
        #Roll the data.
        data = rollList(self._radius_history, -self._last_radius_index)

        #Determine the velocity of the vehicle relative to this object
        return discrete_derivative(data)

    def get_relative_acceleration(self, velocity=None):
        """Determine the acceleration of the vehicle relative to this object"""

        if velocity is None:
            _, velocity = self.get_relative_velocity()

        return discrete_derivative(velocity)

    def plot_history(self):
        """Use pyplot to plot coordinates history in a graph"""
        #FIXME unused variable
        #data = rollList(self._radius_history, -self._last_radius_index)
        vel_x, vel_y = self.get_relative_velocity()
        acc_x, acc_y = self.get_relative_acceleration(vel_y)

        #Scale the velocity
        vel_y = [x / (self._ups/self._bups) for x in vel_y]

        #Plot the data
        #plt.plot(xAxis, data, label="Radius")
        plt.plot(vel_x, vel_y, label="Velocity")
        plt.plot(acc_x, acc_y, label="Acceleration")

        plt.title(str(self))
        plt.xlabel("Sample number")
        plt.legend()
        plt.show()
