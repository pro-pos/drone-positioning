"""Module contains circle class"""

import math
from point import *

class Circle(Point):
    """class models drawn circle in pygame playfield"""

    def __init__(self, position=(0, 0), radius=0):
        super().__init__(position)

        self._radius = radius
        self._intersections = list()

    def set_radius(self, radius):
        """Set radius of the circle object"""
        self._radius = round(radius)

    def get_radius(self):
        """Get radius of the circle object"""
        return self._radius

    def add_intersection(self, point):
        """Add intersection to the list of intersection of the circle object"""
        self._intersections.append(point)

    def get_intersections(self):
        """Return the intersections of the circle object"""
        return self._intersections

    def reset_intersections(self):
        """Empty the intersections list of the object"""
        self._intersections = list()

    def get_closest_intersections(self):
        """Return the two closest intersections"""

        #Pair = (p1, p2)
        pairs = list()

        if len(self._intersections) <= 2:
            return list()

        for intersection in range(0, len(self._intersections) -1):
            this_point = self._intersections[intersection]

            #Get the closest intersection of the remaining intersections
            closest_point = this_point.get_closest_point(self._intersections[intersection+1:])

            if closest_point is not None:
                #Append the pair
                distance = this_point.get_distance_to(closest_point)
                pairs.append([this_point, closest_point, distance])

        #Check if pairs has content
        if len(pairs) == 0:
            return list()

        #Sort the pairs
        pairs.sort(key=lambda x: x[2])

        return pairs[0][:-1]

    def intersect(self, circle):
        """Return the x and y coordinates of the intersection 
        between self and another circle object"""
        x1, y1 = self.get_pos()
        x2, y2 = circle.get_pos()
        r1 = self.get_radius()
        r2 = circle.get_radius()

        dx = x2-x1
        dy = y2-y1
        D = round(math.sqrt(dx**2 + dy**2))

        # Distance between circle centres
        if D > r1 + r2:
            #The circles do not intersect
            return None
        elif D < math.fabs(r2 - r1):
            #No Intersect - One circle is contained within the other
            return None
        elif D == 0 and r1 == r2:
            #No Intersect - The circles are equal and coincident
            return None
        else:
            if D == r1 + r2 or D == r1 - r2:
                case = "The circles intersect at a single point"
            else:
                case = "The circles intersect at two points"

            chorddistance = (r1**2 - r2**2 + D**2)/(2*D)

            # distance from 1st circle's centre to the chord between intersects
            halfchordlength = math.sqrt(r1**2 - chorddistance**2)
            chordmidpointx = x1 + (chorddistance*dx)/D
            chordmidpointy = y1 + (chorddistance*dy)/D

            p1 = (round(chordmidpointx + (halfchordlength*dy)/D),
                  round(chordmidpointy - (halfchordlength*dx)/D))

            theta1 = round(math.degrees(math.atan2(p1[1]-y1, p1[0]-x1)))

            p2 = (round(chordmidpointx - (halfchordlength*dy)/D),
                  round(chordmidpointy + (halfchordlength*dx)/D))

            theta2 = round(math.degrees(math.atan2(p2[1]-y1, p2[0]-x1)))

            if theta2 > theta1:
                p1, p2 = p2, p1

            #return (p1, p2, case)
            return Point(p1), Point(p2)

    def intersect_line(self, point):
        """Draws a line between the center and the point"""

        #Calculate angle
        this_x, this_y = self.get_pos()
        other_x, other_y = point.get_pos()
        delta_x = abs(other_x - this_x)
        delta_y = abs(other_y - this_y)

        if delta_x == 0:
            if this_y > other_y:
                angle = -0.5 * math.pi
            else:
                angle = 0.5 * math.pi
        elif delta_y == 0:
            if this_x > other_x:
                angle = 1 * math.pi
            else:
                angle = 0 * math.pi
        else:
            angle = math.atan(delta_y/delta_x)

        #Calculate intersection point
        intersect_x = (math.cos(angle) * self.get_radius())
        intersect_y = (math.sin(angle) * self.get_radius())

        intersect_x = this_x + (-1 if this_x > other_x else 1) * intersect_x
        intersect_y = this_y + (-1 if this_y > other_y else 1) * intersect_y

        return Point((intersect_x, intersect_y))

    def draw(self, screen, pygame, color=(255, 255, 255)):
        if self.get_radius() < 1:
            return

        #Draw the circle
        pygame.draw.circle(screen, color, self.get_pos(), self.get_radius(), 1)
