# Drone positioning

## Regels, gebruik en setup GIT
[CONTRIBUTING GUIDE](CONTRIBUTING.md)

## Documentatie
### Papers & LaTeX documenten
[Positioning Paper](https://www.overleaf.com/3866657114crxtkxzmrztx)  
[Drone Sensors Paper](https://www.overleaf.com/3888426685pswcqwwwgqkg)  

### Code
Voor code kan de [Doxygen](http://doxygen.nl/) methode worden aangehouden.

## Handige links
[Markdown cheatcheat](https://github.com/tchapi/markdown-cheatsheet)  
[Doxygen](http://doxygen.nl/)  
[The Power of Ten - Rules for Developing Safety Critical Code](https://spinroot.com/gerard/pdf/P10.pdf)  
[ASCII to HEX](https://www.asciitohex.com/)  