#!/bin/sh

function setup {
	echo "Installing python virtual environment.."
	$(python3 -m venv .pyenv)
	echo "Done."
	echo 

	echo "Updating pip.."
	$(pip install --upgrade pip)
	echo "Done."
	echo 



	echo "Installing requirements.."
	cat requirements.txt
	$(pip install -r requirements.txt)



	echo
	echo "Setup done."
}

function update {
	echo "Updating 'requirements.txt'"
	$(pip freeze > requirements.txt)
	cat requirements.txt
	echo "requirements.txt was updated."
}

echo "What action would you like to preform?"
echo
echo "Choose 'Setup' to create a new python virtual environment"
echo "Choose 'Update' to update the 'requirements.txt' file."
echo

select option in "Setup" "Update" "Exit"; do
    case $option in
        Setup ) setup; break;;
	Update ) update; break;;
	Exit ) exit;;
    esac
done
