# Contributing

## Quick start
Om te kunnen beginnen met werken op de repository moet je eerst het project
clonen. Gebruik ``git clone git@gitlab.cmi.hro.nl:project-codema/drone-positioning.git`` en 
``cd drone-positioning``. Gebruik hiervoor een ssh key. 
[Tutorial voor SSH keys](https://gitlab.cmi.hro.nl/help/ssh/README#generating-a-new-ssh-key-pair).  
  
Om ervoor te zorgen dat je commits een naam en email bevatten, gebruik deze commando's:  
``git config --global user.name "[1234567] Voornaam Achternaam"`` en  
``git config --global user.email "1234567@hr.nl"``.   
Verander hierbij het studentnummer en naam. Als je niet wilt dat deze instellingen globaal zijn, 
zorg er dan voor dat je in je terminal in de repository directory bent, en gebruik dan de commando's
zonder de ``--global`` parameter.


## Issue tracking
Gebruik het issue tracking systeem van gitlab voor problemen in de code die je bent tegen gekomen. 
Zodra je een issue hebt gefixt, beschrijf dit in je commit message.  

## Branching
Voor ieder ding wat je doet op de repo maak je een branch aan met ``git checkout -b branchnaam``
waarbij de naam van de branch word bepaald door het type change. Voor features gebruik je de naam
``feature/featurenaam``, voor (bug)fixes gebruik je ``fix/fixnaam``. Voor overige aanpassingen 
zoals aanpassingen in de README.md gebruik je bijvoorbeeld de naam ``other/readme_improvement``.  

## Merge & Pull-Requests
Mergen van branches is mogelijk, behalve naar de ``master`` branch. De ``master`` branch wordt 
door de project maintainer beheerd. Om jouw ``feature`` of ``fix`` branch te kunnen mergen 
met de ``master`` branch maak je een Pull-Request aan. Deze kan worden geaccepteerd door 
de repository maintainer.