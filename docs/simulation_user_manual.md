# Simulation user manual

## Installation
Make sure that python3 is installed.  
  
Run the '``setup.sh``' shell script from within your terminal and choose the setup option. This will create a new environment and install the required packages.


## Usage
Run '``python main.py``' from within your terminal inside the ``src/simulation`` directory.  
  
### Movement
To move, use the keys W, A, S, D on your keyboard.  
W -> Up  
S -> Down  
A -> Left  
D -> Right  

### Pausing the simulation
Use the spacebar on to pause and unpause the simulation.  

### Toggle drone visibility
Use the enter/return key to toggle the visibility of the drone.  

### Radius history
To debug(plot) the beacon's radius history, press the 'P' key.  


## Legend

### Cirle colors
Color | Details
--- | ---
<span style="color:#0000ff">#0000FF (Blue)</span> | Raw radius with error value.
<span style="color:#00ff00">#00FF00 (Green)</span> | Accurate, but slow average radius (error corrected).
<span style="color:#ff0000">#FF0000 (Red)</span> | Less accurate, but fast average radius. A weighted average is taken from the data by using a Sigmoid function.
<span style="color:#c8c800">#FFFF00 (Yellow)</span> | Location accuracy circle. The bigger this circle, the less accurate the position could be determined.

### Dot colors
Color | Details
--- | ---
<span style="color:#000000">#FFFFFF (White)</span> | Beacon or drone.
<span style="color:#c8c800">#C8C800 (Yellow)</span> | Location estimation history.
