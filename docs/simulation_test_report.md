# Simulation test report

## Tests
# | Test | Acceptance criteria | Actions 
--- | --- | --- | ---
1 | Simulation startup | No errors are shown during startup. | Run '``python main.py``'
2 | Simulation runtime | No errors are shown during normal usage (test for 10 minutes). | Run '``python main.py``', wait 10 minutes.
3 | Simulation runtime | Simulation can be paused and unpaused. | Press the space key.
4 | Simulation background | The simulation background equals the floorplan of a greenhouse. | Compare the background to the floorplan of a greenhouse.
5 | Drone movement | Movement works as predicted. | Move the drone by using the W, A, S, D keys.
6 | Drone visibility | The visibility of the drone can be toggled. | Press the return/enter key.
7 | Position algorithm | The location of the drone can be determined. The position is shown. | Move the drone and disable the drone visibility.
8 | Position error algorithm | The location of the drone can still be determined, even if there is some error value. | Move the drone, disable visibility.

## Results
# | Result | Details
--- | --- | ---
1 | <span style="color:green">Accepted</span> | None
2 | <span style="color:green">Accepted</span> | None
3 | <span style="color:green">Accepted</span> | None
4 | <span style="color:red">Not accepted</span> | Another background is shown. The background engine works fine.
5 | <span style="color:green">Accepted</span> | The extra feature 'Acceleration' also works.
6 | <span style="color:green">Accepted</span> | None
7 | <span style="color:green">Accepted</span> | The accurate location can be determined.
8 | <span style="color:green">Accepted</span> | The beacon error is shown with blue circles.
